# BulbReferal

### Introduction

This notebook consist of codes for analysing the Bulb's member referral behaviour.
The data (customer info csv and referral csv can be found in the Data directory)
All plots will be outputted into the Output directory.

To run this notebook, please follow the instructions below:

#### Create a virtual environment

```bash
conda create --name bulb python=3
conda activate bulb
pip install -r requirements.txt
```

#### Create kernel on jupyter notebook
```bash
python -m ipykernel install --user --name bulb --display-name "Python (bulb)"
```

#### Open Jupyter notebook
```bash
jupyter notebook Bulb_member_referrals.ipynb
```

#### Contact
winson88.lam@gmail.com